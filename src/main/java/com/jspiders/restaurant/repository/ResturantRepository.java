package com.jspiders.restaurant.repository;

import java.util.Properties;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import com.jspiders.restaurant.dto.ResturantDto;
import com.jspiders.restaurant.entity.ResturantInfo;
import com.jspiders.restaurant.util.PropertiesUtil;
import com.jspiders.restaurant.util.SessionFactoryUtil;

public class ResturantRepository {

	public void saveResturantDetails(ResturantInfo resturantInfo) {
		try {
			SessionFactory sessionFactory = SessionFactoryUtil.getSessionFactoryObject();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.save(resturantInfo);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public ResturantInfo findResturantById(Long id) {
		try {
			SessionFactory sessionFactory = SessionFactoryUtil.getSessionFactoryObject();
			Session session = sessionFactory.openSession();
			return session.get(ResturantInfo.class, id);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}

	public void updateResturantDeatails(ResturantDto  resturantDto) {
		ResturantInfo resturantInfo = findResturantById(resturantDto.getId());
		if(resturantInfo != null) {
			resturantInfo.setAddress(resturantDto.getAddress());
			resturantInfo.setRaitng(resturantDto.getRating());
			
			SessionFactory sessionFactory = SessionFactoryUtil.getSessionFactoryObject();
			Session session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();
			session.update(resturantInfo);
			transaction.commit();
		}
	}
	
	public void deleteById(Long id) {
		ResturantInfo resturantInfo = findResturantById(id);
		SessionFactory sessionFactory = SessionFactoryUtil.getSessionFactoryObject();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.delete(resturantInfo);
		transaction.commit();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	


}
